import ReactDOM from 'react-dom';
import * as React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import { Component } from 'react-simplified';
import { Chat } from './chat-component';
import { Whiteboard } from './whiteboard-component';
import { Alert, Card, NavBar } from './widgets';

class Menu extends Component {
  render() {
    return (
      <NavBar brand="JPE Collab">
        <NavBar.Link to="/whiteboard">Whiteboard</NavBar.Link>
        <NavBar.Link to="/chat">Chat</NavBar.Link>
      </NavBar>
    );
  }
}
class Home extends Component {
  render() {
    return (
      <Card title="Welcome">
        Welcome to your collaboration platform. You can either share the whiteboard, or chat with
        other users.
      </Card>
    );
  }
}

class WhiteboardComponent extends Component {
  render() {
    return <Whiteboard />;
  }
}

class ChatComponent extends Component {
  render() {
    return <Chat />;
  }
}

ReactDOM.render(
  <>
    <Alert />
    <HashRouter>
      <div>
        <Menu />
        <Route exact path="/" component={Home} />
        <Route exact path="/whiteboard" component={WhiteboardComponent} />
        <Route exact path="/chat" component={ChatComponent} />
      </div>
    </HashRouter>
  </>,
  document.getElementById('root')
);
